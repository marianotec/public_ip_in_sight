# Public Ip in Sight

## Author

> Koremblum Nicolas Mariano (n.m.koremblum@ieee.org)

## Introduction

> This code allows you to get and store, over time, the public ip of a terminal updated in a repository as a string in a txt file

## Installation

> To run the code you will need python3 and to have installed the next pakages: pycurl, python-git

> Edit the "config.py", set your own repository information and edit the properties according to your preferences
