#!/usr/bin/python
import pycurl
import subprocess
from time import strftime, sleep, gmtime
from os import system
from io import BytesIO
from re import findall
from git import Repo
from config import *

c = pycurl.Curl()
buffer = BytesIO()
ip_aux = 0
git_repo = Repo(".")

git_repo.git.config('--global', "user.name", git_my_name)
git_repo.git.config('--global', "user.email", git_my_email)

with open(psw_filepath) as fd:
	git_password = fd.read().strip("\n")

c.setopt(c.URL,"https://ipinfo.io/ip")
c.setopt(c.WRITEDATA, buffer)

process = subprocess.Popen(['/bin/bash'], shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, encoding='utf8')

while 1:
	c.perform()

	# Verifies if it is an IP
	ip = findall( r'[0-9]+(?:\.[0-9]+){3}', str(buffer.getvalue()) )[0]

	if(ip != ip_aux):
		ip_aux = ip

		with open("ip.txt","w") as fd:
			fd.write(ip+"\n")

		commit_str = "\"IP Updated at "+strftime("%Y-%m-%d %H:%M:%S",gmtime())+"\""

		try:
			git_repo.index.add(['*'])

			git_repo.git.commit(m=commit_str)
			
			system("git push https://"+git_username+":"+git_password+"@"+git_webrepopath+" "+git_branch)

		except:
			print("There where some error when commiting...\n")

	sleep(sleep_time_mins*60+sleep_time_secs)
