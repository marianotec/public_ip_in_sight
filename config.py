#!/usr/bin/python
sleep_time_mins = 0
sleep_time_secs = 60  # One minute

# Path to a file that contains your user's password to automatically push the new ip to the repository
psw_filepath = "/home/user/psw.txt"

git_webrepopath = "bitbucket.org/marianotec/public_ip_in_sight.git"
git_username = "marianotec"
git_branch = "master"
git_my_name = "Nicolas Mariano Koremblum"
git_my_email = "n.m.koremblum@ieee.org"
